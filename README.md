# bogo-agent

A simple, but very dumb implementation of an agent that will try all actions randomly.

## CLI args
* `-C`

Your clientID, no default exists as this should be specified uniquely for any user. 

* `-D`

The path to the BDI write directory where the `gpts` amd `logs` files can be found.
Default: `../bdi-interface/` 

* `-L`

The port to connect to the BDI interface on.
Default: `40000`

* `-G` 

The full name of the GPT file to be used.
Default: `gpt.1.xml` 

* `-S`

The seed to be used by the simulation server.
Default: `1`

* `-E`

The seed to be used locally.
Default: `1`

## Example of use

First navigate to the bogo-agent root folder and generate the jars with: `./gradlew fatJar`   
Then run the server following instructions here (stop at step 10, instead of test-agent we will run the bogo-agent): https://gitlab.com/intention-progression-competition/docker-containers/docker-ipc/docker-ipc#docker-server   
Now run the bogo-agent with: `java -jar build/libs/bogo-agent-all-1.0.jar -C bogo`

You should see in the terminal the list of actions executed by the bogo-agent.
