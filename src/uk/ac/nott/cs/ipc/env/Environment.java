package uk.ac.nott.cs.ipc.env;// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------


import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class represents the uk.ac.nott.cs.ipc.env.Environment and contains all related data.
 */
public class Environment {

    private HashMap<String, Action> actions;
    private HashMap<String, Literal> literals;
    private ArrayList<String> stochasticLiterals;

    private HashMap<String, Goal> goals;
    private ArrayList<String> activeGoals;
    private ArrayList<String> completedGoals;

    public Environment() {
        goals = new HashMap<>();
        activeGoals = new ArrayList<>();
        completedGoals = new ArrayList<>();
        stochasticLiterals = new ArrayList<>();
    }

    public HashMap<String, Action> getActions() {
        return actions;
    }

    public void setActions(HashMap<String, Action> actions) {
        this.actions = actions;
    }

    public HashMap<String, Literal> getLiterals() {
        return literals;
    }

    public void setLiterals(HashMap<String, Literal> literals) {
        this.literals = literals;
    }

    public ArrayList<String> getStochasticLiterals() {
        return stochasticLiterals;
    }

    public HashMap<String, Goal> getGoals() {
        return goals;
    }

    public void setGoals(HashMap<String, Goal> goals) {
        this.goals = goals;
    }

    public ArrayList<String> getActiveGoals() {
        return activeGoals;
    }

    public ArrayList<String> getCompletedGoals() {
        return completedGoals;
    }


}
