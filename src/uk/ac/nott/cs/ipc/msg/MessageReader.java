package uk.ac.nott.cs.ipc.msg;// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
//
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;
import uk.ac.nott.cs.ipc.env.Environment;

import javax.xml.parsers.SAXParserFactory;
import java.io.StringReader;

public class MessageReader {

    private Environment env;
    private ContestState state;
    private String lastMsg;
    private SAXParserFactory factory;
    MsgHandler handler;

    public ContestState getState() {
        return state;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public enum ContestState {
        ACTIVE, COMPLETE, TIMEOUT
    }

    public MessageReader(Environment env){
        this.env = env;
        this.factory = SAXParserFactory.newInstance();
        this.handler = new MsgHandler();
    }

    public void parseMessage (String message){
        try {
            factory.newSAXParser().parse(new InputSource(new StringReader(message)), handler);
        } catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }


    class MsgHandler extends DefaultHandler {
        boolean inEnv = false, inLit = false, inGoa = false;
        StringBuffer stringBuffer = new StringBuffer();

        /* <environment>
                <literals>
                    <EV-11>false</EV-11>
                    ...
                    <EV-9>true</EV-9>
                </literals>
                <goals>
                    <T0-G0>false</T0-G0>
                    <T1-G0>false</T1-G0>
                </goals>
            </environment>
            <gptfile>gpts/gpt.2.xml</gptfile>
            <status>
                <contest>ACTIVE</contest>
                <timeremaining>600000</timeremaining>
                <message>Initialisation started</message>
                <command>VALID_COMMAND</command>
            </status>
        */

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {

            if (qName.equalsIgnoreCase("Environment")) {
                inEnv = true;
            } else if (qName.equalsIgnoreCase("Literals")){
                inLit = true;
            } else if (qName.equalsIgnoreCase("Goals")){
                inGoa = true;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if (qName.equalsIgnoreCase("Environment")) {
                inEnv = false;
            } else if (qName.equalsIgnoreCase("Literals")){
                inLit = false;
            } else if (qName.equalsIgnoreCase("Goals")){
                inGoa = false;
            } else if (qName.equalsIgnoreCase("Message")){
                lastMsg = stringBuffer.toString();
            } else if (qName.equalsIgnoreCase("Contest")){
                switch (stringBuffer.toString()){
                    case "ACTIVE":
                        state = ContestState.ACTIVE;
                        break;
                    case "COMPLETE":
                        state = ContestState.COMPLETE;
                        break;
                    case "TIMEOUT":
                        state = ContestState.TIMEOUT;
                        break;
                }
            } else if (inEnv && inLit) {
                env.getLiterals().get(qName).setState(
                        Boolean.parseBoolean(stringBuffer.toString())
                );
            }
            stringBuffer.setLength(0);
        }

        @Override
        public void characters(char ch[], int start, int length) {
            stringBuffer.append(ch, start, length);
        }
    }
}
