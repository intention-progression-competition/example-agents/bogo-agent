package uk.ac.nott.cs.ipc.msg;// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
//
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import uk.ac.nott.cs.ipc.env.Action;

import java.io.StringWriter;

public class MessageWriter {
    String clientid;

    public MessageWriter(String clientid){
        this.clientid = clientid;
    }

    public String initMessage(int serverSeed, String gptfile){
        Element command = new Element("command");
        Document msg = new Document(command);
        command.setAttribute("clientid", clientid);

        Element initiate = new Element("initiate");
        command.addContent(initiate);

        Element seed = new Element("seed");
        seed.addContent(String.valueOf(serverSeed));
        initiate.addContent(seed);

        Element file = new Element("gptfile");
        file.addContent("gpts/" + gptfile);
        initiate.addContent(file);

        return writeOut(msg);
    }

    public String actionMessage(Action action){
        Element command = new Element("command");
        Document msg = new Document(command);
        command.setAttribute("clientid", clientid);

        Element actionMsg = new Element("action");
        actionMsg.addContent(action.getName());
        command.addContent(actionMsg);

        return writeOut(msg);
    }

    public String quitMessage(){
        Element command = new Element("command");
        Document msg = new Document(command);
        command.setAttribute("clientid", clientid);

        Element quit = new Element("quit");
        command.addContent(quit);

        return writeOut(msg);
    }

    private String writeOut(Document doc){
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getCompactFormat());
        StringWriter writer = new StringWriter();
        try {
            outputter.output(doc, writer);
        } catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }
        String proto = writer.toString();
        return proto.replace("\n", "").replace("\r", "");
    }


}
