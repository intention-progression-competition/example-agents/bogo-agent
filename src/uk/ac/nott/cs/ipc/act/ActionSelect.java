package uk.ac.nott.cs.ipc.act;

import uk.ac.nott.cs.ipc.env.Action;

public interface ActionSelect {
    Action pickAction();
}
