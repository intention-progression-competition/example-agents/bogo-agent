package uk.ac.nott.cs.ipc.act;

import uk.ac.nott.cs.ipc.env.Action;
import uk.ac.nott.cs.ipc.env.Environment;

public abstract class AbstractActionSelect implements ActionSelect {
    Environment env;

    AbstractActionSelect (Environment env){
        this.env = env;
    }
}
