package uk.ac.nott.cs.ipc.act;

import uk.ac.nott.cs.ipc.env.Action;
import uk.ac.nott.cs.ipc.env.Environment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DumbActionSelect extends AbstractActionSelect {
    private Random rng;
    public DumbActionSelect(Environment env, Random rng) {
        super(env);
        this.rng = rng;
    }

    @Override
    public Action pickAction() {
        List<Action> acts = new ArrayList<>(this.env.getActions().values());
        return acts.get(rng.nextInt(acts.size()));
    }
}
