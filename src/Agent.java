// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

import uk.ac.nott.cs.ipc.act.ActionSelect;
import uk.ac.nott.cs.ipc.act.DumbActionSelect;
import uk.ac.nott.cs.ipc.env.Action;
import uk.ac.nott.cs.ipc.env.Environment;
import uk.ac.nott.cs.ipc.msg.GPTParser;
import uk.ac.nott.cs.ipc.msg.MessageReader;
import uk.ac.nott.cs.ipc.msg.MessageWriter;

import java.io.*;
import java.net.Socket;
import java.util.Random;

public class Agent {

    public static void main(String[] args) throws IOException {

        final String interfaceHostName = "127.0.0.1";

        int BDIPortNumber = 40000;
        int simulatorSeed = 1;
        int localSeed = 1;
        Random rng;
        String GPTFile = "gpt-testclient.xml";
        String BDIDir = "test-gpts/";
        String CLIENTID = null;

        Environment env = new Environment();


        int i = 0;
        String arg;

        while (i < args.length && args[i].startsWith("-")) {
            arg = args[i++];
            if (arg.length() != 2) {
                System.out.println(arg + " is not a valid flag");
                System.exit(1);
            }

            char flag = arg.charAt(1);
            switch (flag) {
                case 'L': // BDI port
                    BDIPortNumber = Integer.parseInt(args[i++]);
                    break;
                case 'G': // GPT file
                    GPTFile = args[i++];
                    break;
                case 'S': // Simulation seed
                    simulatorSeed = Integer.parseInt(args[i++]);
                    break;
                case 'E': // Local seed
                    localSeed = Integer.parseInt(args[i++]);
                    break;
                case 'D': // BDI File directory
                    BDIDir = args[i++];
                    break;
                case 'C': // clientid
                    CLIENTID = args[i++];
                    break;
            }
        }

        if (CLIENTID == null){
            System.out.println("Please specify your clientID.");
            System.exit(1);
        }
        rng = new Random(localSeed);

        Socket socket = new Socket(interfaceHostName, BDIPortNumber);

        PrintWriter serverOut = new PrintWriter(socket.getOutputStream(), true);
        MessageWriter messageWriter = new MessageWriter(CLIENTID);

        BufferedReader serverIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        MessageReader messageReader = new MessageReader(env);
        try{
            System.out.println("Connected on " + BDIPortNumber);
            String msgOut = messageWriter.initMessage(simulatorSeed, BDIDir + GPTFile);
            serverOut.println(msgOut);
            System.out.println("Initing");

            String msgIn = serverIn.readLine();
            System.out.println(msgIn);

            String filepath = BDIDir + GPTFile;
            GPTParser gptParser = new GPTParser(env);
            gptParser.parseGPTFile(filepath);

            messageReader.parseMessage(msgIn);
            System.out.println(messageReader.getLastMsg());

            ActionSelect selector = new DumbActionSelect(env,rng);
            Action workingAction;
            i = 0;
            while(messageReader.getState() == MessageReader.ContestState.ACTIVE){
                workingAction = selector.pickAction();
                System.out.println(workingAction.getName());
                msgOut = messageWriter.actionMessage(workingAction);
                serverOut.println(msgOut);

                msgIn = serverIn.readLine();
                messageReader.parseMessage(msgIn);
                System.out.println(">" + messageReader.getLastMsg());

                i++;
            }

            msgOut = messageWriter.quitMessage();
            serverOut.println(msgOut);
            System.out.println("Quiting after " + i + " cycles on: " + messageReader.getState());

            System.exit(0);

        } catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }

    }
}
